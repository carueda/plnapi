package plnapi

import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import plnapi.data.{DbFactory, DbInterface}
import plnapi.model._
import plnapi.server._


class PlnApiSpec extends WordSpec with Matchers with ScalatestRouteTest with Service {
  val dbFactory: DbFactory = new DbFactory
  val db: DbInterface = dbFactory.testDb
  //val db: DbInterface = new plnapi.data.DbMem

  override def beforeAll(): Unit = {
    super.beforeAll()
    // any db initialization?
    logger.debug(db.details)
  }

  override def afterAll(): Unit = {
    db.close()
    super.afterAll()
  }

  "Token endpoint" should {

    "list all tokens" in {
      Get("/api/token") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val tokens = responseAs[Seq[Token]]
        tokens.length should be >= 0
      }
    }

    var tkidOpt: Option[String] = None

    val platform_name = "SomePlatform"
    val start         = "SomeStart"
    val end           = "SomeEnd"
    val state         = "SomeState"
    val description   = "SomeDescription"

    "add a new token" in {
      val tka = TokenAdd(
        platform_name = platform_name,
        start = start,
        end = end,
        state = state,
        description = description
      )
      Post(s"/api/token", tka) ~> routes ~> check {
        status shouldBe Created
        contentType shouldBe `application/json`
        val tk = responseAs[Token]
        tkidOpt = Some(tk.tkid)
        tk.platform_name === platform_name
        tk.start         === start
        tk.end           === end
        tk.state         === state
        tk.description   === description
      }
    }

    "get specific token" in {
      println(s"getting token ${tkidOpt.get}")
      Get(s"/api/token/${tkidOpt.get}") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val tk = responseAs[Token]
        tk.tkid === tkidOpt.get
      }
    }

    "update token" in {
      val tku = TokenUpdate(
        description = Some("updatedDescription")
      )
      Put(s"/api/token/${tkidOpt.get}", tku) ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val tk = responseAs[Token]
        tk.platform_name === platform_name
        tk.start         === start
        tk.end           === end
        tk.state         === state
        tk.description   === tku.description
      }
    }

    "delete token" in {
      Delete(s"/api/token/${tkidOpt.get}") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val tk = responseAs[Token]
      }
    }

  }

  "Plan endpoint" should {
    "respond with all plans" in {
      pending
    }
  }
}

