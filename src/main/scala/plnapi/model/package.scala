package plnapi

package object model {

  case class Token(
                    tkid:           String,
                    platform_name:  String,
                    start:          String,
                    end:            String,
                    state:          String,
                    description:    String

                    //,ttype:        {type: String, default: "ttdeployment", enum: ['ttdeployment', 'ttmission'] }
                    //,geometry:     {}
                    //,_updated:     { on: Date, by: {type: String, default: "" } }
                  )

  // generic error
  case class GnError(code: Int,
                     msg: String,
                     tkid: Option[String] = None
                    )

  object GnErrorF {
    def tokenDefined(tkid: String): GnError =
      GnError(409, "token already defined", tkid = Some(tkid))

    def tokenUndefined(tkid: String): GnError =
      GnError(404, "token undefined", tkid = Some(tkid))

  }

}
