package plnapi

import fansi.Color.Red
import plnapi.config.configFile
import plnapi.server.Server

object Main {
  def main(args: Array[String]): Unit = {
    if (args.contains("run-server")) {
      if (!configFile.canRead) {
        System.err.println(Red(s"cannot access $configFile"))
      }
      else new Server().run(keyToStop = !args.contains("-d"))
    }
    else {
      System.err.println(
        s"""
           |Usage:
           |   plnapi run-server [-d]
        """.stripMargin)
    }
  }
}
