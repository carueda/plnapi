package plnapi.data

import com.typesafe.scalalogging.{LazyLogging ⇒ Logging}
import plnapi.config.{MongoCfg, cfg}

import scala.concurrent.ExecutionContext

class DbFactory(implicit ec: ExecutionContext) extends Logging {

  def openDb: DbInterface = {
    cfg.mongo.map(new MongoDb(_))
      .getOrElse(new DbMem)
  }

  def testDb: DbInterface = {
    val host = sys.env.getOrElse("MONGO_HOST", "localhost")
    new MongoDb(MongoCfg(
      uri      = s"mongodb://$host",
      database = "plnapi_test"
    ))
  }
}
