package plnapi.data

import plnapi.model._
import plnapi.server.{TokenAdd, TokenUpdate}

import scala.concurrent.Future

trait DbInterface {

  def details: String

  def listTokens: Future[Seq[Token]]

  def addToken(tka: TokenAdd): Future[Either[GnError, Token]]

  def getToken(tkid: String): Future[Option[Token]]

  def updateToken(tkid: String, tku: TokenUpdate): Future[Either[GnError, Token]]

  def deleteToken(tkid: String): Future[Either[GnError, Token]]

  def close(): Unit
}
