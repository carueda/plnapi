package plnapi.data

import com.typesafe.scalalogging.{LazyLogging ⇒ Logging}
import org.mongodb.scala._
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import plnapi.config.MongoCfg
import plnapi.model._
import plnapi.server.{TokenAdd, TokenUpdate}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}

object MongoDb {
  case class MgToken(
                      _id:            ObjectId,
                      platform_name:  String,
                      start:          String,
                      end:            String,
                      state:          String,
                      description:    String

                      //,ttype:        {type: String, default: "ttdeployment", enum: ['ttdeployment', 'ttmission'] }
                      //,geometry:     {}
                      //,_updated:     { on: Date, by: {type: String, default: "" } }
                    )

  object MgToken {
    def apply(
               platform_name: String,
               start: String,
               end: String,
               state: String,
               description: String
             ): MgToken =
      new MgToken(new ObjectId(),
        platform_name, start, end, state, description)
  }
}

class MongoDb(mCfg: MongoCfg) extends DbInterface with Logging {
  import MongoDb._

  val details: String = s"MongoDb-based database (uri: ${mCfg.uri} db: ${mCfg.database})"

  def listTokens: Future[Seq[Token]] = {
    tokens.find().map(mtk2tk).toFuture()
  }

  def addToken(tka: TokenAdd): Future[Either[GnError, Token]] = {
    val p = Promise[Either[GnError, Token]]()

    //println(s"MongoDb: addToken: saving ...")
    val mtk = tka2mtk(tka)
    tokens.insertOne(mtk).subscribe(new Observer[Completed] {
      override def onNext(result: Completed): Unit = {
        println(s"MongoDb: addToken: saved.")
        p.success(Right(mtk2tk(mtk)))
      }

      override def onError(e: Throwable): Unit = {
        println(s"MongoDb: addToken: error: ${e.getMessage}")
        p.success(Left(GnError(500, s"error inserting token: ${e.getMessage}")))
      }

      override def onComplete(): Unit = {
        //println(s"MongoDb: addToken: onComplete.")
      }
    })

    p.future
  }

  def getToken(tkid: String): Future[Option[Token]] = {
    logger.debug(s"getToken: tkid=$tkid")
    tokens.find(equal("_id", new ObjectId(tkid)))
      .first().map(mtk2tk).toFuture().map(_.headOption)
  }

  def updateToken(tkid: String, tku: TokenUpdate): Future[Either[GnError, Token]] = {
    val p = Promise[Either[GnError, Token]]()
    getToken(tkid) map {
      case Some(tk) ⇒
        val updates = Seq(
          tku.platform_name.map(set("platform_name", _)),
          tku.start.map(        set("start", _)),
          tku.end.map(          set("end", _)),
          tku.state.map(        set("state", _)),
          tku.description.map(  set("description", _))
        ).flatten

        tokens.updateOne(equal("_id", new ObjectId(tkid)),
          combine(updates: _*)
        ).toFuture().map { x ⇒
          p.success(Right(tk))
        }

      case None ⇒
        p.success(Left(GnError(404, "Not registered", tkid = Some(tkid))))
    }
    p.future
  }

  def deleteToken(tkid: String): Future[Either[GnError, Token]] = {
    val p = Promise[Either[GnError, Token]]()
    getToken(tkid) map {
      case Some(tk) ⇒
        tokens.deleteOne(equal("_id", new ObjectId(tkid))).toFuture().map { x ⇒
          p.success(Right(tk))
        }

      case None ⇒
        p.success(Left(GnError(404, "Not registered", tkid = Some(tkid))))
    }
    p.future
  }

  def close(): Unit = ()

  private def mtk2tk(mtk: MgToken): Token = {
    Token(
      mtk._id.toHexString,
      mtk.platform_name,
      mtk.start,
      mtk.end,
      mtk.state,
      mtk.description
    )
  }

  private def tka2mtk(tka: TokenAdd): MgToken = {
    MgToken(
      new ObjectId(),
      tka.platform_name,
      tka.start,
      tka.end,
      tka.state,
      tka.description
    )
  }

  private val mongoClient: MongoClient = MongoClient(mCfg.uri)

  private val database: MongoDatabase = {
    import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
    import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
    import org.mongodb.scala.bson.codecs.Macros._

    val codecRegistry = fromRegistries(
      fromProviders(
        createCodecProviderIgnoreNone[MgToken](),
        //classOf[MgToken]
        //,classOf[MgPlan]
        //,classOf[JsObject]
        //,classOf[JsString]
        //,classOf[JsNumber]
      ),
      DEFAULT_CODEC_REGISTRY
    )
    mongoClient.getDatabase(mCfg.database).withCodecRegistry(codecRegistry)
  }

  private val tokens: MongoCollection[MgToken] = database.getCollection("tokens")
}
