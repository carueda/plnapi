package plnapi.data
import java.util.UUID

import plnapi.model._
import plnapi.server.{PlnApiJsonImplicits, TokenAdd, TokenUpdate}

import scala.concurrent.{ExecutionContext, Future}

class DbMem(implicit ec: ExecutionContext) extends DbInterface with PlnApiJsonImplicits {
  def details: String = "dbMem"

  def listTokens: Future[Seq[Token]] = Future {
    tokenMap.values.toSeq
  }

  def addToken(tka: TokenAdd): Future[Either[GnError, Token]] = Future {
    val tk = tka2tk(tka)
    tokenMap += tk.tkid → tk
    Right(tk)
  }

  def getToken(tkid: String): Future[Option[Token]] = Future {
    tokenMap.get(tkid)
  }

  def updateToken(tkid: String, tku: TokenUpdate): Future[Either[GnError, Token]] = Future {
    tokenMap.get(tkid) match {
      case Some(tk) ⇒
        var ntk = tk
        tku.platform_name foreach { x ⇒ ntk = ntk.copy(platform_name = x) }
        tku.start foreach         { x ⇒ ntk = ntk.copy(start = x) }
        tku.end foreach           { x ⇒ ntk = ntk.copy(end = x) }
        tku.state foreach         { x ⇒ ntk = ntk.copy(state = x) }
        tku.description foreach   { x ⇒ ntk = ntk.copy(description = x) }

        tokenMap.update(tkid, ntk)
        Right(ntk)

      case None ⇒ Left(GnError(404, "Not registered", tkid = Some(tkid)))
    }
  }

  def deleteToken(tkid: String): Future[Either[GnError, Token]] = Future {
    tokenMap.remove(tkid) match {
      case Some(tk) ⇒
        Right(tk)

      case None ⇒ Left(GnError(404, "Not registered", tkid = Some(tkid)))
    }
  }

  def close(): Unit = ()

  private def tka2tk(tka: TokenAdd): Token = {
    Token(
      randomId,
      tka.platform_name,
      tka.start,
      tka.end,
      tka.state,
      tka.description
    )
  }

  private def randomId: String = UUID.randomUUID().toString

  import scala.collection.mutable.{Map ⇒ MMap}

  private val tokenMap: MMap[String, Token] = MMap.empty
}
