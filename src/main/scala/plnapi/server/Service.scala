package plnapi.server

import javax.ws.rs.Path

import akka.http.scaladsl.server.{Directives, Route}
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import io.swagger.annotations._
import plnapi.model._


trait Service extends TokenService {

  def routes: Route = {
    cors()(SwaggerSpecService.routes) ~
    swaggerUi ~
    staticRoute ~
    tkRoute
  }

  private val staticRoute: Route = {
    get {
      getFromResourceDirectory("")
    }
  }

  private val swaggerUi: Route =
    path("apidoc") { getFromResource("swaggerui/index.html") } ~
      getFromResourceDirectory("swaggerui")
}

@Api(produces = "application/json")
@Path("/token")
trait TokenService extends ServiceImpl with PlnApiJsonImplicits with Directives {
  def tkRoute: Route = {
    tkList ~
    tkAdd ~
    tkGet ~
    tkUpdate ~
    tkDelete
  }

  @ApiOperation(value = "Get tokens", nickname = "getTokens",
    tags = Array("token"),
    httpMethod = "GET",
    response = classOf[Array[Token]]
  )
  def tkList: Route = {
    path("api" / "token") {
      get {
        complete {
          db.listTokens
        }
      }
    }
  }

  @ApiOperation(value = "Add a token", nickname = "addToken",
    tags = Array("token"),
    httpMethod = "POST", code = 201,
    response = classOf[Token])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "body", value = "token definition", required = true,
      dataTypeClass = classOf[Token], paramType = "body")
  ))
  //@ApiResponses(Array(
  //  new ApiResponse(code = 409, message = "Token already registered")
  //))
  def tkAdd: Route = path("api" / "token") {
    (post & entity(as[TokenAdd])) { tka ⇒
      complete {
        addToken(tka)
      }
    }
  }

  @ApiOperation(value = "Get a token", nickname = "getToken",
    tags = Array("token"),
    httpMethod = "GET",
    response = classOf[Token]
  )
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined token")
  ))
  def tkGet: Route = pathPrefix("api" / "token" / Segment) { tkid ⇒
    cors() {
      get {
        complete {
          getToken(tkid)
        }
      }
    }
  }

  @ApiOperation(value = "Update a token", nickname = "updateToken",
    tags = Array("token"),
    httpMethod = "PUT",
    response = classOf[Token]
  )
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined token")
  ))
  def tkUpdate: Route = path("api" / "token" / Segment) { tkid ⇒
    (put & entity(as[TokenUpdate])) { tka ⇒
      complete {
        updateToken(tkid, tka)
      }
    }
  }

  @ApiOperation(value = "Delete a token", nickname = "deleteToken",
    tags = Array("token"),
    httpMethod = "DELETE",
    response = classOf[Token]
  )
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined token")
  ))
  def tkDelete: Route = pathPrefix("api" / "token" / Segment) { tkid ⇒
    cors() {
      delete {
        complete {
          getToken(tkid)
        }
      }
    }
  }

}
