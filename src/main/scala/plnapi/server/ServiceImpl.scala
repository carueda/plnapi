package plnapi.server

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model._
import com.typesafe.scalalogging.{LazyLogging ⇒ Logging}
import plnapi.data.DbInterface
import plnapi.model.{GnError, GnErrorF}
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


trait ServiceImpl extends PlnApiJsonImplicits with Logging {
  def db: DbInterface

  def addToken(tka: TokenAdd): Future[ToResponseMarshallable] = {
    logger.debug(s"addToken: tka=$tka")
    db.addToken(tka) map {
      case Right(tk) ⇒
        goodResponse(StatusCodes.Created, tk.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def getToken(tkid: String): Future[ToResponseMarshallable] = {
    logger.debug(s"getToken: tkid=$tkid")
    db.getToken(tkid) map {
      case Some(tk) ⇒ tk
      case None     ⇒ errorResponse(GnErrorF.tokenUndefined(tkid))
    }
  }

  def updateToken(tkid: String, tku: TokenUpdate): Future[ToResponseMarshallable] = {
    logger.debug(s"updateToken: tku=$tku")
    db.updateToken(tkid, tku) map {
      case Right(tk) ⇒
        goodResponse(StatusCodes.OK, tk.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def deleteToken(tkid: String): Future[ToResponseMarshallable] = {
    logger.debug(s"getToken: tkid=$tkid")
    db.deleteToken(tkid) map {
      case Right(tk) ⇒
        goodResponse(StatusCodes.OK, tk.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  private def goodResponse(status: StatusCode, jsonValue: JsValue): HttpResponse = {
    HttpResponse(
      status = status,
      entity = HttpEntity(
        ContentType(MediaTypes.`application/json`),
        jsonValue.compactPrint
      )
    )
  }

  private def errorResponse(error: GnError): HttpResponse = {
    HttpResponse(
      status = error.code,
      entity = HttpEntity(
        ContentType(MediaTypes.`application/json`),
        error.toJson.compactPrint
      )
    )
  }
}
