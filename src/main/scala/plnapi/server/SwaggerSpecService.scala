package plnapi.server

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import plnapi.config.cfg

object SwaggerSpecService extends SwaggerHttpService {
  override val info = Info(
    version = cfg.plnapi.version,
    title = "Planning API",
    description =
      "(under construction)"
  )

  // the url of your api, not swagger's json endpoint.
  // Note that no scheme should be included.
  override val host: String = cfg.externalUrl.replaceFirst("^https?://", "")

  override val basePath: String = "/api"    //the basePath for the API you are exposing

  override val apiDocsPath: String = "apidoc" //where you want the swagger-json endpoint exposed

  override val apiClasses: Set[Class[_]] = Set(
    classOf[TokenService],
  )
}
