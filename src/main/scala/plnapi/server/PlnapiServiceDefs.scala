package plnapi.server

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import plnapi.model._
import spray.json.DefaultJsonProtocol

case class TokenAdd(
                     platform_name:  String,
                     start:          String,
                     end:            String,
                     state:          String,
                     description:    String
                   )

case class TokenUpdate(
                        platform_name:  Option[String] = None,
                        start:          Option[String] = None,
                        end:            Option[String] = None,
                        state:          Option[String] = None,
                        description:    Option[String] = None
                      )


trait PlnApiJsonImplicits extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val _tka       = jsonFormat5(TokenAdd)
  implicit val _tku       = jsonFormat5(TokenUpdate)
  implicit val _tk        = jsonFormat6(Token)

  implicit val _gnError   = jsonFormat3(GnError)
}
