package plnapi.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import plnapi.config.cfg
import plnapi.data._

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}

class Server extends Service {
  val dbFactory: DbFactory = new DbFactory()(ExecutionContext.global)
  val db: DbInterface = dbFactory.openDb

  implicit val as: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContextExecutor = as.dispatcher
  implicit val mt: ActorMaterializer = ActorMaterializer()

  def run(keyToStop: Boolean): Unit = {
    println(db.details)
    val bindingFuture = Http().bindAndHandle(routes, cfg.httpInterface, cfg.httpPort)
    println(s"plnapi server online at ${cfg.httpInterface}:${cfg.httpPort}/")
    if (keyToStop) {
      import scala.io.StdIn
      println("Press RETURN to stop...")
      StdIn.readLine()
      // trigger unbinding from the port and shutdown when done
      bindingFuture
        .flatMap(_.unbind())
        .onComplete { _ ⇒
          db.close()
          as.terminate()
        }
    }
  }
}
