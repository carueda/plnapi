package plnapi

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import fansi.Color.Yellow

package object config {
  val configFile: File = new File("./conf", "plnapi.conf")
  lazy val tsConfig: Config = {
    println(Yellow(s"Configuration file: ${configFile.getCanonicalPath}"))
    ConfigFactory.parseFile(configFile)
      .withFallback(ConfigFactory.load()).resolve()
  }
  lazy val cfg: PlnApiCfg = PlnApiCfg(tsConfig)

}
