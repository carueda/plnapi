package plnapi.config

import carueda.cfg._

@Cfg
case class PlnApiCfg(
                      externalUrl:   String = "http://localhost:5050",
                      httpInterface: String = "0.0.0.0",
                      httpPort:      Int    = 5050,
                      mongo:         Option[MongoCfg]
                    ) {
  object plnapi {
    val version: String = $  // from reference.conf
  }
}

@Cfg
case class MongoCfg(
                     uri:       String   = "mongodb://localhost",
                     database:  String   = "plnapi"
                   )
