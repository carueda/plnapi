lazy val plnapiVersion = getVersion

val akkaHttpCorsV = "0.2.2"
val akkaHttpV     = "10.0.10"
val cfgV          = "0.0.7"
val logbackV      = "1.2.3"
val mongoV        = "2.1.0"
val pprintV       = "0.5.2"
val scalaLoggingV = "3.7.2"
val scalatestV    = "3.0.3"
val scalaV        = "2.12.4"
val swaggerAkkaV  = "0.11.0"

name := "plnapi"

version := plnapiVersion

scalaVersion := scalaV

libraryDependencies ++= Seq(
  "com.typesafe.akka"    %% "akka-http"            % akkaHttpV,
  "com.typesafe.akka"    %% "akka-http-spray-json" % akkaHttpV,
  "com.typesafe.akka"    %% "akka-http-testkit"    % akkaHttpV,
  "ch.megard"            %% "akka-http-cors"       % akkaHttpCorsV,
  "com.github.carueda"   %% "cfg"                  % cfgV % "provided",
  "org.mongodb.scala"    %% "mongo-scala-driver"   % mongoV,
  "org.scalatest"        %% "scalatest"            % scalatestV % "test"

  ,"com.lihaoyi"         %% "pprint"               % pprintV

  ,"com.github.swagger-akka-http" %% "swagger-akka-http" % swaggerAkkaV

  ,"com.typesafe.scala-logging"   %% "scala-logging"   % scalaLoggingV
  ,"ch.qos.logback"                % "logback-classic" % logbackV
)
addCompilerPlugin(
  ("org.scalameta" % "paradise" % "3.0.0-M10").cross(CrossVersion.full)
)
mainClass in assembly := Some("plnapi.Main")
assemblyJarName in assembly := s"plnapi-$plnapiVersion.jar"

def getVersion: String = {
  val version = {
    val refFile = file("src/main/resources/reference.conf")
    val versionRe = """plnapi\.version\s*=\s*(.+)""".r
    IO.read(refFile).trim match {
      case versionRe(v) ⇒ v
      case _ ⇒ sys.error(s"could not parse plnapi.version from $refFile")
    }
  }
  println(s"plnapi.version = $version")
  version
}
